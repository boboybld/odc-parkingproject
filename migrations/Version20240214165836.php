<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240214165836 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE categorie_places (id INT AUTO_INCREMENT NOT NULL, prix BIGINT NOT NULL, libele VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE clients (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(100) NOT NULL, prenom VARCHAR(255) NOT NULL, adresse VARCHAR(255) NOT NULL, telephone VARCHAR(50) NOT NULL, numpiece VARCHAR(100) NOT NULL, sexe VARCHAR(5) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE places (id INT AUTO_INCREMENT NOT NULL, reservation_id INT DEFAULT NULL, categorie_id INT NOT NULL, numero VARCHAR(100) NOT NULL, statut TINYINT(1) NOT NULL, INDEX IDX_FEAF6C55B83297E7 (reservation_id), INDEX IDX_FEAF6C55BCF5E72D (categorie_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reservation (id INT AUTO_INCREMENT NOT NULL, cout BIGINT NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', description LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE voitures (id INT AUTO_INCREMENT NOT NULL, client_id INT NOT NULL, matricule VARCHAR(50) NOT NULL, marque VARCHAR(255) NOT NULL, modele VARCHAR(100) NOT NULL, couleur VARCHAR(100) NOT NULL, INDEX IDX_8B58301B19EB6921 (client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE places ADD CONSTRAINT FK_FEAF6C55B83297E7 FOREIGN KEY (reservation_id) REFERENCES reservation (id)');
        $this->addSql('ALTER TABLE places ADD CONSTRAINT FK_FEAF6C55BCF5E72D FOREIGN KEY (categorie_id) REFERENCES categorie_places (id)');
        $this->addSql('ALTER TABLE voitures ADD CONSTRAINT FK_8B58301B19EB6921 FOREIGN KEY (client_id) REFERENCES clients (id)');
        $this->addSql('DROP TABLE articles');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE articles (id INT AUTO_INCREMENT NOT NULL, titre VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, description LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE places DROP FOREIGN KEY FK_FEAF6C55B83297E7');
        $this->addSql('ALTER TABLE places DROP FOREIGN KEY FK_FEAF6C55BCF5E72D');
        $this->addSql('ALTER TABLE voitures DROP FOREIGN KEY FK_8B58301B19EB6921');
        $this->addSql('DROP TABLE categorie_places');
        $this->addSql('DROP TABLE clients');
        $this->addSql('DROP TABLE places');
        $this->addSql('DROP TABLE reservation');
        $this->addSql('DROP TABLE voitures');
    }
}
